import Command.*;
import Observer.CommentaryObject;
import Observer.Observer;
import Observer.Subject;
import Observer.TVUsers;
import Observer.Commentary;
import Responsibility.*;
import State.Player;
import State.UI;
import Strategy.RemoteDevice;
import Strategy.TVPost1;
import Strategy.TVPost2;
import Strategy.TVPost3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class Main {

    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private static Server server;
    private static void init() {
        server = new Server();
        server.register("admin@gmail.com", "admin_pass");
        server.register("socol.maxim@rts.one", "user_pass");

        // Проверки связаны в одну цепь. Клиент может строить различные цепи,
        // используя одни и те же компоненты.
        Middleware middleware = new ThrottlingMiddleware(2);
        middleware.linkWith(new UserExistsMiddleware(server))
                .linkWith(new RoleCheckMiddleware());

        // Сервер получает цепочку от клиентского кода.
        server.setMiddleware(middleware);
    }

    public static void main(String[] args) throws IOException {

        //        Command Pattern
        System.out.println("Command Pattern... ");
        TV tv = new TV();
        Command connectCommand = new IsConnecting(tv);
        Command disconnectCommand = new IsNotConnecting(tv);
        Switch tvSwitching = new Switch(connectCommand, disconnectCommand);
        tvSwitching.Connect();
        tvSwitching.Disconnect();
        System.out.print("\n");

//        Observer Pattern

        System.out.println("Observer Pattern... ");
        Subject subject = new CommentaryObject(new ArrayList<Observer>(), "Soccer Match [Barcelona - Real Madrid]");
        Observer observer = new TVUsers(subject, "Adam Warner [New York]");
        observer.subscribe();

        System.out.println();

        Observer observer2 = new TVUsers(subject, "Tim Ronney [London]");
        observer2.subscribe();

        Commentary cObject = ((Commentary)subject);
        cObject.setDesc("Welcome to live Soccer match");
        cObject.setDesc("Current score 0-0");

        System.out.println();

        observer2.unSubscribe();

        System.out.println();

        cObject.setDesc("It's a goal!!");
        cObject.setDesc("Current score 1-0");

        System.out.println();

        Observer observer3 = new TVUsers(subject, "Marrie [Paris]");
        observer3.subscribe();

        System.out.println();

        cObject.setDesc("It's another goal!!");
        cObject.setDesc("Half-time score 2-0");
        System.out.print("\n");

//        Strategy pattern
        System.out.println("Strategy pattern...");
        RemoteDevice remoteDevice = new RemoteDevice(new TVPost1());
        System.out.println("TV1 shows: " + remoteDevice.executeStrategy(1));
        remoteDevice = new RemoteDevice(new TVPost2());
        System.out.println("TV2 shows: " + remoteDevice.executeStrategy(2));
        remoteDevice = new RemoteDevice(new TVPost3());
        System.out.println("TV3 shows: " + remoteDevice.executeStrategy(3));
        System.out.print("\n");


        //        //State pattern
        System.out.println("State pattern...");
        Player player = new Player();
        UI ui = new UI(player);
        ui.init();
        System.out.print("\n");


        //       Chain of Responsibility pattern
        System.out.println("Chain of Responsibility pattern...");
        init();

        boolean success;
        do {
            System.out.print("Enter email: ");
            String email = reader.readLine();
            System.out.print("Input password: ");
            String password = reader.readLine();
            success = server.logIn(email, password);
        } while (!success);

    }
}
