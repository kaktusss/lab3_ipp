package Responsibility;

/**
 * Конкретный элемент цепи обрабатывает запрос по-своему.
 */
public class RoleCheckMiddleware extends Middleware {
    public boolean check(String email, String password) {
        if (email.equals("admin@gmail.com")) {
            System.out.println("Hello, Admin!");
            return true;
        }
        System.out.println("Hello, Socol Maxim!");
        return checkNext(email, password);
    }
}