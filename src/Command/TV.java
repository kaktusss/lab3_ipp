package Command;

public class TV {
    private int volume = 0;
    public void connect() {
        volume += 18;
        System.out.print("TV Connected, ");
        System.out.println("Volume: " + volume);
    }
    public void disconnect() {
        volume -= 18;
        System.out.print("TV Not Connected, ");
        System.out.println("Volume: " + volume);
    }
}
