package Strategy;

public interface Strategy {
    String TvShow(int buttonNumber);
}